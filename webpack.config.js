const currentTask = process.env.npm_lifecycle_event
const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const {resolve} = require("path");
const PACKAGE = require('./package.json');
const webpack = require('webpack');
const version = PACKAGE.version;

const config = {
  performance: {
    hints: false
  },
  entry: {
    "main": resolve(__dirname, './src/index.tsx')
  },
  output: {
    filename: `[name]-${version}.js`,
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ["*", ".js", ".jsx", ".ts", ".tsx"],
  },
  plugins: [
    new HtmlWebpackPlugin({template: "./src/index.html"}),
    new webpack.DefinePlugin({
      process: {env: {REACT_APP_CREDENTIAL_DATA:{}}}
    }),
    new CopyWebpackPlugin(
      {
        patterns: [
          {
            from: path.resolve(__dirname, 'src/assets'),
            to: path.resolve(__dirname, 'dist/assets')
          },
          {
            from: path.resolve(__dirname, 'src/vendor'),
            to: path.resolve(__dirname, 'dist/vendor')
          }
        ]
      }
    ),
    new ForkTsCheckerWebpackPlugin()
  ],
  mode: "development",
  devServer: {
    historyApiFallback: true,
    port: 8080,
    hot: true,
    static: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: ['./src/sass'], // Erlaubt Imports aus `src/styles`
              },
              additionalData: `@use '02-config/typographie.scss' as *;`
            }
          },
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        exclude: /node_modules/,
        type: 'asset/resource',
        generator: {
          filename: './fonts/[name][ext]',
        },
      },
      {
        test: /\.(jpg|jpeg)$/,
        exclude: /node_modules/,
        type: 'asset/resource',
        generator: {
          filename: './assets/[name][ext]',
        },
      },
      {
        test: /\.(js|(j|t)sx?)$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              ['@babel/preset-env', {"useBuiltIns": "usage", "corejs": 3, "targets": "defaults"}],
              '@babel/preset-react',
              '@babel/preset-typescript'
            ]
          }
        }
      }
    ]
  },
  mode: 'development'
}

if(currentTask === 'dev') {
  config.devtool = "source-map"
}

if(currentTask == 'build') {
  config.mode = "production"
  config.module.rules[0].use[0] = MiniCssExtractPlugin.loader
  config.plugins.push(new MiniCssExtractPlugin({ filename: 'main.css'}), new CleanWebpackPlugin(), new WebpackManifestPlugin())
}

module.exports = config
