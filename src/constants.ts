import React from "react";

export const DEV = window.location.host === "localhost:8080";
export const SERVER = window.location.protocol + "//" + window.location.host;

const paths = window.location.pathname.split('/').filter( el => { return el !== ""});
export const BASE = DEV ? "" : paths[0];

export interface TileValues {
  classname:string,
  value: string,
  ballValueRef?:string
}

export interface SingleTile {
  type: TileValues,
  left: number,
  top: number,
  x: number,
  y: number,
  id: string,
  key: string,
  ref: any
}

export interface Rect {
  x:number,
  y:number,
  width: number,
  height: number
}

export interface RectSet {
  top: Rect,
  right: Rect,
  bottom: Rect,
  left: Rect
}

export interface HighscoreEntry {
  name: string,
  score: number
}

export interface Level {
  id: number,
  levelstr: string,
  bonustime: number,
  proofed:boolean,
  levelType:string

}

export const COLS = 22;
export const ROWS = 13;
export const TILE_WIDTH = 35;
export const TILE_HEIGHT = 40;


export enum Playstate {
  PLAYING,
  IDLE,
  CALCBONUS,
  COMPLETED,
  FINISHED
}

export enum Collision {
  TOP = 1,
  RIGHT = 2,
  BOTTOM = 3,
  LEFT = 4
}

export enum LevelListType {
  CUSTOMLEVELS = "customlevels",
  GAMELEVELS = "gamelevels"
}

export enum LevelListId {
  CUSTOMLEVELS = 0,
  GAMELEVELS = 1
}

export const  STAGE_WIDTH = COLS * TILE_WIDTH;
export const  STAGE_HEIGHT = ROWS * TILE_HEIGHT;


export const TILE_BRICK_GREY:TileValues = { classname: 'brick', value: "A"};
export const TILE_BRICK_BLUE:TileValues = { classname: 'blue', value: "B", ballValueRef: "ball blue"};
export const TILE_BRICK_RED:TileValues = { classname: 'red', value: "C"};
export const TILE_BRICK_GREEN:TileValues = { classname: 'green', value: "D", ballValueRef: "ball green"};
export const TILE_CONVERTER_BLUE:TileValues = { classname: 'blue-change', value: "J", ballValueRef: "ball blue"};
export const TILE_CONVERTER_GREEN:TileValues = { classname: 'green-change', value: "K", ballValueRef: "ball green"};
export const TILE_MOVER_BLUE:TileValues = { classname: 'blue-move', value: "T", ballValueRef: "ball blue"};
export const TILE_MOVER_GREEN:TileValues = { classname: 'green-move', value: "U", ballValueRef: "ball green"};
export const TILE_SPACE:TileValues = { classname: 'space', value: "Z"};
export const TILE_BALL_BLUE:TileValues = { classname: 'ball blue', value: "1"};
export const TILE_BALL_GREEN:TileValues = { classname: 'ball green', value: "2"};

export const BALL_LIST = [TILE_BALL_BLUE.value, TILE_BALL_GREEN.value]
export const POINT_BRICK_LIST = [TILE_BRICK_BLUE.value, TILE_BRICK_GREEN.value];
export const CONVERTER_BRICK_LIST = [TILE_CONVERTER_BLUE.value, TILE_CONVERTER_GREEN.value];
export const MOVER_BRICK_LIST = [TILE_MOVER_BLUE.value, TILE_MOVER_GREEN.value];

export const TILE_TYPE_LIST = [
  TILE_SPACE,
  TILE_BRICK_GREY,
  TILE_BRICK_BLUE,
  TILE_BRICK_RED,
  TILE_BRICK_GREEN,
  TILE_CONVERTER_BLUE,
  TILE_CONVERTER_GREEN,
  TILE_MOVER_BLUE,
  TILE_MOVER_GREEN,
  TILE_BALL_BLUE,
  TILE_BALL_GREEN,
];


export const defaultGameLevels = [
  {
    id: 1,
    levelstr: "AAAAAAAZZCCCZZZAAAAAAAABBCBBAZZZZZZZZABZKZBAABZZZBAZZZZZZZZABZZZBAABZZZBAZZZZZZZZABZZZBAAAATAAAZZZZZZZZAAATAAAZZZZZZZZZZZZZZZZZZZZZZZZZZZZ1ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZAAAUAAAZZZZZZZZAAAUAAAAZZZZZAZZZZZZZZADZZZDAAZZZZZAZZZZZZZZADZZZDAAZZZZZAZZZZZZZZADZJZDAAAAAAAAZZZZZZZZAAAAAAA",
    bonustime: 100,
    proofed: true,
    levelType: LevelListType.GAMELEVELS
  },
  {
    id: 2,
    levelstr: "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZBBZZZZZZZZZZZZZZZZZZZBBBBZZZZZZZZZZZZZZZZZDDDDDDZZZZZZZZZZZZZZZDDDDDDDKZZZZZZZZZZZZZCCCCCCCCCCZZZZZZZZZZZZZDDDDDDDDZZZZZZZZZZZZZZZDDDDDDZZZZZZZZZZZZZZZZZBBBBZZZZZZZZZZZZZZZZZZZBBZZZZZ1ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ",
    bonustime: 100,
    proofed: true,
    levelType: LevelListType.GAMELEVELS
  },
  {
    id: 3,
    levelstr: "ZZZZZZZZZZZZZZZZZZZZZZAAAAAAAAAAAAAAAZZZZZZZZBBBBBBBZBBBBBAZZZZZZZZBBBBBBBBBBBZBAZZZZZZZZBBBBZBBBBZBBBAZZZZZZZZBBBBBBBBBBBBBAZZZZZZZZKKKKKKKKKKKKKAZZZZZZZZAAAAAAAAAAAAAAZZZZZZZZUZZZUZZZUZZZUAZZZZZZZZZUZUZUZUZUZUZAZZZZZZZJZZKZZZUZZZCZ2AZZZZZZZZZUZUZUZUZUZUZAZZZZZZZZDZZZDZZZDZZZUAZZZZZZZ",
    bonustime: 100,
    proofed: true,
    levelType: LevelListType.GAMELEVELS
  },
  {
    id: 4,
    levelstr:   "ZZZZZZZZZZZZZZZZZZZZZZZZBBBZDDDZDDDZBBBZZZZZZZBZBZZDZZZDZZBZBZZZZZZZBZBZZDZZZDZZBZBZZZZZZZBBBZZDZZZDZZBBBZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZAAAAAAAAADDAAAAAAAAAAAZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZJZCZCZCZCZZCZCZCZCZCZKAAAAAAAAA1ZAAAAAAAAAAA",
    bonustime: 100,
    proofed: true,
    levelType: LevelListType.GAMELEVELS
  },
  {
    id: 5,
    levelstr: "AAAAAAAZZZZZZZZAAAAAAAABBCBBAZZZZZZZZABZKZBAABZZZBAZZZZZZZZABZZZBAABZZZBAZZZZZZZZABZZZBAAAATAAAZZZZZZZZAAATAAAZZZZZZZZZZZZZZZZZZZZZZZZZZZZ1ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZAAAUAAAZZZZZZZZAAAUAAAAZZZZZAZZZZZZZZADZZZDAAZZZZZAZZZZZZZZADZZZDAAZZZZZAZZZZZZZZADZJZDAAAAAAAAZZZZZZZZAAAAAAA",
    bonustime: 100,
    proofed: false,
    levelType: LevelListType.CUSTOMLEVELS
  },
  {
    id: 6,
    levelstr: "JZZZKZZZBZZZBZZZKZZZZZZZZZBZZZKZZZBZZZBZZZZZZZZZBZZZCZZZKZZZBZZZZZZZZZKZZZBZZZBZZZKZZZZZZZZZBZZZKZZZBZZZBZZZZZZZZZBZZZBZZZKZZZBZZZZZZZZZKZZZBZZZCZZZKZZZZZZZZZBZZZKZZZBZZZBZZZZZZZZZBZZZBZZZKZZZBZZZZZZZZZKZZZBZZZBZZZKZZZZZZZZZCZZZKZZZBZZZCZZZZZZZZZBZZZBZZZKZZZBZZZZZ1ZZZKZZZBZZZBZZZKZZZZZ",
    bonustime: 100,
    proofed: false,
    levelType: LevelListType.CUSTOMLEVELS
  }
];