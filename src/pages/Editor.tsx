import React, {ChangeEvent, useEffect, useState} from "react";
import {
  BALL_LIST,
  COLS, defaultGameLevels, Level, LevelListId, LevelListType,
  POINT_BRICK_LIST,
  ROWS, SingleTile,
  TILE_HEIGHT,
  TILE_SPACE,
  TILE_TYPE_LIST,
  TILE_WIDTH
} from "../constants";
import Tile from "../components/Tile";
import {useTileManager} from "../hooks/useTileManager";
import {Button, Alert} from "@mui/material";
import DialogLayer from "../components/DialogLayer";
import {useLocalStorage} from "../hooks/useLocalStorage";
import {Tabs, Tab} from "@mui/material";
import {CustomTabPanel} from "../components/CustomTabPanel";
import LevelPreviewList from "../components/LevelPreviewList";
import {useTranslation} from "react-i18next";
import {useLevels} from "../hooks/useLevels";


interface ValidState {
  pointTileSet: boolean,
  ballSet: boolean
}

export interface DialogState {
  message: string,
  payload: number|null
}

const Editor = () => {

  const {t} = useTranslation();
  const {getGameLevels, getCustomLevels} = useLevels();

  const [levelTypeSelection, setLevelType] = useState<string>(LevelListType.CUSTOMLEVELS);
  const [levels, setLevels] = useLocalStorage("levels", defaultGameLevels);

  const {getPreview, isBall} = useTileManager();
  const [validState, setValidState] = useState<ValidState>({pointTileSet: false, ballSet: false});
  const [tiles, setTiles] = useState<SingleTile[]>(getPreview(Array(ROWS*COLS).fill(TILE_SPACE.value).join('')));
  const [editTile, setEditTile] = useState(TILE_SPACE);
  const [editLevel, setEditLevel] = useState<null | Level>(null); // index of editLevels
  const [editMode, setEditMode] = useState(false);
  const [dialogState, setDialogState] = useState<DialogState>({message: '', payload: null});
  const [tabValue, setTab] = React.useState(0);
  const [bonusTime, setBonusTime] = useState<number>(100);

  useEffect( () => {
    setBonusTime(editLevel ? editLevel.bonustime : 100)
  }, [editLevel])

  useEffect(() => {
    const levelList = tiles.map( t => {return t.type.value;});
    const pointBricks = levelList.filter( t => {
      return (POINT_BRICK_LIST.indexOf(t) !== -1);
    });

    const ballSet = levelList.filter( t => {
      return (BALL_LIST.indexOf(t) !== -1);
    });

    setValidState({pointTileSet: pointBricks.length > 0, ballSet: ballSet.length > 0});
  }, [tiles, editLevel]);

  const resetLevel = ():void => {
    setTiles(getPreview(Array(ROWS*COLS).fill(TILE_SPACE.value).join('')));
    setEditLevel(null);
  }

  const changeToGameLevel = (id:number) => {
    setLevels(levels.map((lev:Level) => {
      if(lev.id === id) { lev.levelType = LevelListType.GAMELEVELS}
      return lev;
    }));
  }

  const selectLevel = (level:Level) => {
    setTiles(getPreview(level.levelstr));
    setEditLevel(level);
  }

  const saveLevels = (levels:Level[]) => {
    setLevels(levels);
  }

  const changeBonusTime = (evt:ChangeEvent<HTMLInputElement>) => {
    const time = evt.target.value === "" ? 0 : parseInt(evt.target.value);
    setBonusTime(time);
  }

  const deleteLevel = (level:Level) => {
    setDialogState({...dialogState, message: ''})
    saveLevels( levels.filter( (lev:Level) => { return lev.id !== level.id }));

    if (editLevel && editLevel.id === level.id) {
      setEditLevel(null);
    }
  }

  const saveLevel = () => {
    const levelStr = tiles.map( t => {return t.type.value;}).join('');
    const updateLevels = editLevel !== null ?
      levels.map( (lev:Level) => {return editLevel.id === lev.id ? { id: editLevel.id, levelstr: levelStr, bonustime: bonusTime, proofed: false, levelType: LevelListType.CUSTOMLEVELS} : {...lev} })
      : [...levels, { id: levels.reduce( (a:number, c:Level) => { return Math.max(a,c.id);}, 0) + 1, levelstr: levelStr, bonustime: bonusTime, proofed: false, levelType: LevelListType.CUSTOMLEVELS}];

    saveLevels(updateLevels);

    // if it's the first level
    if(editLevel === null) {
      setEditLevel(updateLevels[updateLevels.length-1]);
    }
  }

  const onStartEditMode = (xpos:number, ypos:number) => {
    setEditMode(true);
    changeTile(xpos, ypos, true)
  }

  const onStopEditMode = () => {
    setEditMode(false);
  }

  const changeEditTile = (tile:any) => {
    setEditTile(tile);
  }

  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setTab(newValue);
    console.log(newValue);
    switch (newValue) {
      case LevelListId.CUSTOMLEVELS: setLevelType(LevelListType.CUSTOMLEVELS); break;
      case LevelListId.GAMELEVELS: setLevelType(LevelListType.GAMELEVELS);  break;
      default: ;
    }
    setLevels(levels);
  };

  const changeTile = (xpos:number, ypos:number, forceEdit = false) => {
    if (editMode || forceEdit) {
      const newTiles = tiles.map( tile => {
        if (tile.x === xpos && tile.y === ypos) {
          const isBallType = isBall(editTile.value);
          if (isBallType) {
            resetBallTile();
          }
          tile.type = editTile;
          tile.left = (tile.x * TILE_WIDTH) + (isBallType ? Math.round(TILE_WIDTH/2) : 0);
          tile.top = (tile.y * TILE_HEIGHT) + (isBallType ? Math.round(TILE_HEIGHT/2) : 0);
        }
        return tile;
      });
      setTiles(newTiles);
    }
  }

  const resetBallTile = () => {
    tiles.map(
      tile => {
        if (isBall(tile.type.value)) {
          tile.type = TILE_SPACE;
          tile.left = tile.x * TILE_WIDTH;
          tile.top = tile.y * TILE_HEIGHT;
        }
      }
    );
  }

  return <>
    <section className={'editor'}>
      <h1>Editor</h1>

      <h2 className={'fs-h3'}>{t('blockSelection')}</h2>
      <p>{t('editorManual')}</p>
      <div className={'tile-selection'}>
        {Object.keys(TILE_TYPE_LIST).map( (key:any, index) => { return <a onClick={() => changeEditTile(TILE_TYPE_LIST[key])} key={key} className={ `tile ${TILE_TYPE_LIST[key].classname} ${editTile === TILE_TYPE_LIST[key] ? 'selected' : ''}` }></a> })}
      </div>

      <section className={'canvas-grid'}>
        <div className='canvas mb-m'  data-testid={"canvas-tiles"}>
          {tiles?.map( (t) => {
            return <Tile onChangeTile={() => { changeTile(t.x, t.y)}}
                         onStartEditMode={() => { onStartEditMode(t.x, t.y)}}
                         onStopEditMode={onStopEditMode}
                         key={t.key}
                         left={t.left}
                         top={t.top}
                         type={t.type}/>
          }
          )}
        </div>

        <aside>
          <Alert severity={validState.ballSet ? 'success' : 'error'} className={'mb-m'}>
            <span>{t('ballSet')}: </span>
            <span data-testid={'ball-set'}>{validState.ballSet ? t('yes') : t('no')}</span>
          </Alert>

          <Alert severity={validState.pointTileSet? 'success' : 'error'} className={'mb-m'}>
            <span>{t('pointsSet')}: </span>
            <span data-testid={'points-set'}>{validState.pointTileSet ? t('yes') : t('no')}</span>
          </Alert>

          <h3 className={'mb-xs'}>Bonuszeit</h3>
          <input className={'mb-m'} type={'text'} value={bonusTime} onChange={(e:ChangeEvent<HTMLInputElement>) => { changeBonusTime(e);}}/>

          <Button disabled={!validState.ballSet || !validState.pointTileSet} size={'medium'} className={'full-w mb-m'} variant="contained" onClick={ () => saveLevel()}>Level {editLevel !== null ? 'aktualisieren' : 'speichern'}</Button>
          <Button size={'medium'} className={'full-w mb-m'} variant="contained" onClick={ () => resetLevel()}>Level zurücksetzen</Button>
        </aside>

      </section>

      {dialogState.message !== '' &&
        <DialogLayer dialogState={dialogState}
                     onConfirm={(level: Level) => deleteLevel(level)}
                     onCancel={() => setDialogState({...dialogState, message: ''})}/>
      }


      <Tabs value={tabValue} onChange={handleChange} aria-label="Tabs">
        <Tab label="Eigene Levels" {...a11yProps(LevelListId.CUSTOMLEVELS)} />
        <Tab label="Game Levels" {...a11yProps(LevelListId.GAMELEVELS)} />
      </Tabs>

      <CustomTabPanel value={tabValue} index={LevelListId.CUSTOMLEVELS}>
        <LevelPreviewList levels={getCustomLevels(levels)} levelType={levelTypeSelection} selectLevel={ (level:Level) => selectLevel(level)} setDialogState={ (obj:DialogState) => setDialogState(obj)} changeToGameLevel={ (id:number) => { changeToGameLevel(id)}}/>
      </CustomTabPanel>

      <CustomTabPanel value={tabValue} index={LevelListId.GAMELEVELS}>
        <LevelPreviewList levels={getGameLevels(levels)} levelType={levelTypeSelection} selectLevel={ (level:Level) => selectLevel(level)} setDialogState={ (obj:DialogState) => setDialogState(obj)} changeToGameLevel={ (id:number) => { }} />
      </CustomTabPanel>

    </section>
    </>
}

export default Editor;