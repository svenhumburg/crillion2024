import React, {useCallback, useEffect, useMemo, useReducer, useRef, useState} from "react";
import {
  Collision,
  COLS,
  defaultGameLevels,
  HighscoreEntry,
  Level, LevelListType,
  Playstate,
  ROWS,
  SingleTile,
  STAGE_HEIGHT,
  STAGE_WIDTH,
  TILE_HEIGHT,
  TILE_SPACE,
  TILE_WIDTH,
  TileValues
} from "../constants";
import Ball from "../components/Ball";
import Message from "../components/Message";
import {useParams} from "react-router";
import {useTileManager} from "../hooks/useTileManager";
import {Alert, Button, Card} from "@mui/material";
import {AccessTime, Person, Scoreboard, VideogameAsset, ViewModule} from "@mui/icons-material";
import GameTile from "../components/GameTile";
import {useLocalStorage} from "../hooks/useLocalStorage";
import {useAuth} from "../AuthProvider";
import Highscore from "../components/Highscore";
import {isTouchDevice} from "../utils";
import {useTranslation} from "react-i18next";
import {useLevels} from "../hooks/useLevels";


let _ = require('lodash')

interface BallAttributes {
  top: number,
  left: number,
  color: string,
  direction: number,
  horizontalFactor: number
}

interface GameData {
  matrix: string[][],
  tiles: SingleTile[],
  level: number,
  points: number,
  bonus: number,
  lives: number,
  delayUpdate: boolean,
  remaining: number,
  ballAttr: BallAttributes,
  ballInitialLeft: number,
  ballInitialTop: number,
  ballInitialColor: string,
  showBall:boolean,
  playState:number
}


const gameDataInit = {
  matrix: [],
  tiles: [],
  level: 0,
  points: 0,
  bonus: 100,
  lives: 3,
  delayUpdate: false,
  remaining: 0,
  ballAttr: {top: 0, left: 0, color: '', direction: 0, horizontalFactor: 0},
  ballInitialLeft: 0,
  ballInitialTop: 0,
  ballInitialColor: '',
  showBall: true,
  playState: Playstate.IDLE
}

const Game = () => {
  const { id, levelType } = useParams();
  const {t} = useTranslation();
  const {getGameLevels, getLevelById} = useLevels();
  const [highscore, setHighscore] = useLocalStorage('highscore', []);
  const [levels, setLevels] = useLocalStorage("levels", defaultGameLevels);

  const gameLevels = useMemo(() => {
    return id !== null && id !== undefined ? getLevelById(levels, parseInt(id)) : getGameLevels(levels);
  }, [levels])

  const { user } = useAuth();
  const [gameData, setGameData] = useReducer((state:GameData, item:Partial<GameData>) => { return {...state, ...item } }, gameDataInit);
  const [message, setMessage] = useState('');
  const leftButton = useRef<HTMLButtonElement>(null);
  const rightButton = useRef<HTMLButtonElement>(null);
  const ballRef = useRef<any>(null);
  const { getTileByValue, isPointTile, isBall, getOrCreateRef } = useTileManager();
  let currTiles = [...gameData.tiles];
  let remaining = gameData.remaining;

  /**
   * useEffect hooks
   */

  useEffect( () => {
    if (id) {
      setGameData({ level: 0 });
    }
    initLevel();
  }, [gameData.level])


  useEffect(() => {
    if(gameData.playState !== Playstate.IDLE) {
      if (gameData.lives > 0) {
        restartGame();
      } else {
        onEnd();
      }
    }
  }, [gameData.lives]);


  useEffect(() => {
    if (gameData.remaining === 0 && gameData.playState == Playstate.PLAYING) {
      setMessage(t('messageLevelSuccess'));
      setGameData({playState: Playstate.IDLE})

      setTimeout( ()=> {
        gameData.bonus ? setGameData({playState: Playstate.CALCBONUS}) : setCompleted();
      }, 2000);
    }
  }, [gameData.remaining]);


  useEffect(() => {
    if(gameData.bonus > 0 && (gameData.playState === Playstate.PLAYING || gameData.playState === Playstate.CALCBONUS)) {
      setBonusTimer();
    } else if (gameData.bonus === 0 && gameData.playState === Playstate.CALCBONUS) {
      setCompleted();
    }
  }, [gameData.bonus, gameData.playState]);


  useEffect(() => {
    if(!id && gameData.playState === Playstate.COMPLETED) {
      setMessage('');
      const hasMoreLevels = gameData.level + 1 <= gameLevels.length - 1;
      hasMoreLevels ? setGameData({level: gameData.level + 1}) : onEnd();
    } else if (id && gameData.playState === Playstate.COMPLETED && gameLevels[0].levelType === LevelListType.CUSTOMLEVELS) {
      // set level as proofed
      setMessage( t('messageLevelValidated'));
      setLevels(levels.map((lev:Level) => {
        if(lev.id === parseInt(id)) { lev.proofed = true}
        return lev;
      }));
    }
  }, [gameData.playState])


  const initLevel = useCallback(() => {
    let count = 0;
    const tiles = [];
    const matrix:string[][] = [];
    let remaining = 0;

    for (let y = 0; y < ROWS; y++) {
      for (let x = 0; x < COLS; x++) {
        const type = getTileByValue(gameLevels[gameData.level].levelstr.charAt(count));
        const tile = { type: type, left: x * TILE_WIDTH, top: y * TILE_HEIGHT, x: x, y: y, id: x + "_" + y, key: x + "_" + y, ref:  getOrCreateRef(count)};
        //const tile = { type: type, left: x * (100/COLS), top: y * (100/ROWS), x: x, y: y, id: x + "_" + y, key: x + "_" + y, ref:  getOrCreateRef(count)};
        const isBallTile = isBall(type.value);
        count++;

        if (isBallTile) {
          const ballInitialTop = (y * TILE_HEIGHT) + TILE_HEIGHT/2;
          const ballInitialLeft = (x * TILE_WIDTH) + Math.round(TILE_WIDTH/2);

          //const ballInitialTop = (y * (100/ROWS)) + (100/ROWS)/2;
          //const ballInitialLeft = (x * (100/COLS)) + (100/COLS)/2;

          const ballInitialColor = type.classname;
          const ballAttr =  {
            top: ballInitialTop,
            left: ballInitialLeft,
            color: ballInitialColor,
            direction: 1,
            horizontalFactor: 1
          }

          setGameData({ballInitialTop: ballInitialTop, ballInitialLeft: ballInitialLeft, ballInitialColor: ballInitialColor, ballAttr: ballAttr});
        } else {
          if (type.value !== TILE_SPACE.value) {
            tiles.push(tile);
          }

          if (isPointTile(type.value)) {
            remaining++;
          }
        }

        if (!matrix[y]) {
          matrix[y] = [];
        }

        matrix[y][x] = !isBallTile ? type.value : TILE_SPACE.value;
      }
    }
    setGameData({remaining: remaining, matrix: matrix, tiles: tiles, bonus: gameLevels[gameData.level].bonustime, showBall: true, playState: Playstate.PLAYING});
    setMessage('');
  }, [gameData.level]);


  /**
   * Common functions
   */

  const setBonusTimer = () => {
    if (gameData.bonus > 0 && (gameData.playState === Playstate.PLAYING || gameData.playState === Playstate.CALCBONUS)) {
      setTimeout( () => {
        const data:Partial<GameData> = { bonus : gameData.bonus - 1};

        if (gameData.playState === Playstate.CALCBONUS) {
          data.points = gameData.points + 2;
        }
        setGameData(data);
      },gameData.playState === Playstate.CALCBONUS ? 5 : 1000)
    }
  }


  const setCompleted = () => {
    setTimeout( () => {
      setGameData({playState: Playstate.COMPLETED});
    },1000)
  }


  const restartGame = () => {
    let ballAttr =  {
      top: gameData.ballInitialTop,
      left: gameData.ballInitialLeft,
      color: gameData.ballInitialColor,
      direction: 1,
      horizontalFactor: 1
    }
    setGameData({ballAttr: ballAttr, showBall: true});
    setMessage('');
  }


  const onPoints = (tile:SingleTile) => {
    remaining = remaining - 1;
    setGameData({points: gameData.points + 5, remaining: remaining, showBall: remaining !== 0});
    onRemoveTile(tile);
  }


  const onLostLife = (tile:SingleTile) =>  {
    onRemoveTile(tile);
    setGameData({ showBall: false})
    setMessage(gameData.lives - 1 > 0 ? t('messageLostLife') : t('messageGameOver'))

    setTimeout( ()=> {
      setGameData({ lives: gameData.lives - 1})
    }, 2000);
  }


  const onEnd = () => {
    highscore.push( { name: user ? user : "Unknown User", score: gameData.points} as HighscoreEntry);
    setHighscore(highscore.sort( (a:any,b:any) => b.score - a.score ));
    setGameData({playState: Playstate.FINISHED});
  }


  const onRemoveTile = (tile:SingleTile) => {
    tile.type = { classname: tile.type.classname + " hit", value: tile.type.value};
    currTiles = currTiles.filter(item => { return !(item.x === tile.x && item.y === tile.y)});
    gameData.matrix[tile.y][tile.x] = TILE_SPACE.value;
    setGameData({tiles: currTiles});
  }


  /**
   * GAME EVENTS
   */

  const onControl = (code:string) => {
    if(ballRef && ballRef.current) {
      ballRef.current.doControl(code);
    }
  }

  const onBallColorChange = (classname:string) => {
    let ballAttr = gameData.ballAttr;
    ballAttr.color = classname;
    setGameData({ballAttr: ballAttr});
  }

  const onCollisionMove = (x:number, y:number, collision_direction:number, type:TileValues, tile:SingleTile) => {
    let target_type;
    let newMatrix:string[][]  = _.cloneDeep(gameData.matrix);

    switch(collision_direction) {
      case Collision.TOP: target_type = y + 1 < ROWS ? newMatrix[y + 1][x] : null; break;
      case Collision.RIGHT: target_type = x - 1 >= 0 ? newMatrix[y][x-1] : null; break;
      case Collision.BOTTOM: target_type = y - 1 >= 0 ?  newMatrix[y-1][x] : null; break;
      case Collision.LEFT: target_type = x + 1 < COLS ? newMatrix[y][x + 1] : null; break;
    }

    if (target_type !== null && target_type === TILE_SPACE.value) {
      newMatrix[y][x] = TILE_SPACE.value;

      switch(collision_direction) {
        case Collision.TOP:
          tile.y = y + 1;
          tile.top += TILE_HEIGHT;
          newMatrix[y + 1][x] = type.value;
          break;
        case Collision.RIGHT:
          tile.x = x - 1;
          tile.left -= TILE_WIDTH;
          newMatrix[y][x - 1] = type.value;
          break;
        case Collision.BOTTOM:
          tile.y = y - 1;
          tile.top -= TILE_HEIGHT;
          newMatrix[y - 1][x] = type.value;
          break;
        case Collision.LEFT:
          tile.x = x + 1;
          tile.left += TILE_WIDTH;
          newMatrix[y][x + 1] = type.value;
          break;
      }
    }
    setGameData({matrix: newMatrix});
  }

  const onBallPositionChange = (y:number, x:number, arr_x:number, arr_y:number) => {
    const radius =  4;
    let ballAttr = gameData.ballAttr;
    ballAttr.left = x;
    ballAttr.top = y;

    if (gameData.delayUpdate) {
      setGameData({delayUpdate : false});
      ballAttr.horizontalFactor = 1;
    }

    if (y + radius >= STAGE_HEIGHT) {
      ballAttr.direction = -1;
    } else if (y -  radius <= 0) {
      ballAttr.direction = 1;
    }

    if (x - radius <= 0) {
      ballAttr.horizontalFactor = -1;
      ballAttr.left += 3;
      setTimeout( () => { setGameData({delayUpdate : true}); }, 60);
    } else if (x +  radius >= STAGE_WIDTH) {
      ballAttr.horizontalFactor = -1;
      ballAttr.left -= 3;
      setTimeout( () => { setGameData({delayUpdate : true}); }, 60);
    }

    const rect = {x: ballAttr.left - 4, y: ballAttr.top - 4, width: 10, height: 10};
    const filtered = gameData.tiles.filter( (tile) => { return (arr_x + 1 === tile.x  || arr_x - 1 === tile.x || tile.x === arr_x) && (tile.y === arr_y - 1 || tile.y === arr_y + 1 || tile.y === arr_y)  ;});

    filtered.forEach( tile => {
      if (!tile.ref || !tile.ref.current) return;

      const collision = tile.ref.current.checkCollision(rect, gameData.ballAttr.color);

      if (collision) {
        switch(collision) {
          case Collision.TOP: ballAttr.direction = -1; break;
          case Collision.RIGHT: ballAttr.horizontalFactor = -1; ballAttr.left += 3; setTimeout( () => { setGameData({delayUpdate : true}); }, 60); break;
          case Collision.BOTTOM: ballAttr.direction = 1; break;
          case Collision.LEFT: ballAttr.horizontalFactor = -1; ballAttr.left -= 3; setTimeout( () => { setGameData({delayUpdate : true}); }, 60);break;
        }
      }
    });
    setGameData({ballAttr : ballAttr});
  }

  return <>
    {gameData.playState !== Playstate.FINISHED && <section className={'canvas-grid'}>
        <div className='canvas'>
          {gameData.tiles.map((t) => {
              return <GameTile ref={t.ref}
                               onPoints={() => {
                                 onPoints(t)
                               }}
                               onLostLife={() => {
                                 onLostLife(t)
                               }}
                               onChangeColor={(classname: string) => onBallColorChange(classname)}
                               onCollisionCheck={(x: number, y: number, cd: number, tp: TileValues) => {
                                 onCollisionMove(x, y, cd, tp, t)
                               }}
                               key={t.key}
                               left={t.left}
                               top={t.top}
                               xpos={t.x}
                               ypos={t.y}
                               type={t.type}/>
            }
          )}
          {gameData.showBall &&
          <Ball
                ref={ballRef}
                left={gameData.ballAttr.left}
                top={gameData.ballAttr.top}
                direction={gameData.ballAttr.direction}
                horizontalFactor={gameData.ballAttr.horizontalFactor}
                classname={gameData.ballAttr.color}
                onPositionChange={(y: number, x: number, arr_x: number, arr_y: number) => onBallPositionChange(y, x, arr_x, arr_y)}/>
          }

          { message !== '' &&  <Message classname={'open'} messageText={message}/>}

        </div>

        <aside>
          <Alert className={'mb-xs'} icon={<VideogameAsset/>} severity={'info'}>Level: {gameData.level + 1}</Alert>
          <Alert className={'mb-xs'} icon={<Scoreboard/>} severity={'info'}>{t('points')}: {gameData.points}</Alert>
          <Alert className={'mb-xs'} icon={<Person/>} severity={'info'}>{t('lives')}: {gameData.lives}</Alert>
          <Alert className={'mb-xs'} icon={<ViewModule/>} severity={'info'}>{t('blocks')}: {gameData.remaining}</Alert>
          <Alert className={'mb-xs'} icon={<AccessTime/>} severity={'info'}>Bonus: {gameData.bonus}</Alert>
        </aside>
      {isTouchDevice() &&
        <section>
          <Button className={'control-btn'} variant="contained" size="large" ref={leftButton}
                  onTouchStart={e => onControl('left')} onTouchEnd={e => onControl('')}>LEFT</Button>&nbsp;&nbsp;
          <Button className={'control-btn'} variant="contained" size="large" ref={rightButton}
                  onTouchStart={e => onControl('right')} onTouchEnd={e => onControl('')}>RIGHT</Button>
        </section>
      }
     </section>
    }

    { gameData.playState === Playstate.FINISHED &&
      <>
        <Card variant={'outlined'} className={'mb-m'}>

          <div className={'highscore-form'}>
            <div>Punkte: {gameData.points}</div>

            {/*<form action={''}>
              <div className={'mb-m'}>
                <div className={'form-row'}>
                  <Input label={'Name'}
                         prop={'name'}
                         type={'text'}
                         required={true}
                         value={''}
                         setFormData={(obj: Partial<FormData>) => {
                         }}
                         onChange={(obj: Partial<FormData>) => {
                         }}/>
                </div>
              </div>

              <Button size={'small'} variant="contained" onClick={e => {
              }}>Eintragen</Button>
            </form>*/}
          </div>

        </Card>


        <Card className={'mb-m'}>
          <Highscore/>
        </Card>


        <Button size={'small'} variant="contained" onClick={() => {
          setGameData(gameDataInit);
          initLevel();
        }}>Erneut spielen</Button>
      </>
    }
  </>
}

export default Game;