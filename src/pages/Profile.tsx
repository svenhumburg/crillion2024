import React, {useMemo} from "react";
import {useLocalStorage} from "../hooks/useLocalStorage";
import {defaultGameLevels, Level, LevelListId, LevelListType} from "../constants";
import {Card} from "@mui/material";
import Tile from "../components/Tile";
import {useTileManager} from "../hooks/useTileManager";
import { Draggable } from "react-drag-reorder";
import {useLevels} from "../hooks/useLevels";

function Profile() {
  const {getPreview} = useTileManager();
  const { getGameLevels } = useLevels();

  const [levels, setLevels] = useLocalStorage("levels", defaultGameLevels);
  const gameLevels = useMemo( () => {
    return getGameLevels(levels);
  }, [levels])

  const onReorder = (currPos:number, newPos:number) => {
      [levels[currPos], levels[newPos]] = [levels[newPos], levels[currPos]];
      setLevels(levels);
  }

  return <>
    <span>Game Levels anordnen per Drag & Drop</span>
      { levels.length > 0 &&
        <section className={'preview-outer'}>
          <div className={'preview-grid compact'}>
            <Draggable onPosChange={(currPos, newPos) => onReorder(currPos, newPos)}>
            {levels.map( (level:Level, i:number) => {
              return level.levelType === LevelListType.GAMELEVELS ? <div data-id={i} className={'preview'} key={'preview_' + i}>
                <Card variant={'outlined'} className='canvas small'>
                  {getPreview(level.levelstr).map( (t:any, index) => {
                      return <Tile key={t.key}
                                   left={t.left}
                                   top={t.top}
                                   type={t.type}/>
                    }
                  )}
                </Card>
              </div> : '';
            })}
            </Draggable>
          </div>
        </section>
      }
    </>
}

export default Profile;