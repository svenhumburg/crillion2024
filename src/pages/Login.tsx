import React, {useReducer, useRef} from "react";
import {useAuth} from "../AuthProvider";
import {Button} from "@mui/material";
import Input from "../components/Input";
import Select from "../components/Select";


type FormData = {
  name: string,
  title: string
}

const Login = () =>  {
  const {login} = useAuth();
  const [formData, setFormData] = useReducer( (state:FormData, item:Partial<FormData>) => { return {...state, ...item } }, { name: '', title: ''});
  const formRef = useRef(null);
  const nameRef = useRef<any>(null);
  const refList = [nameRef];

  const doLogin = () => {
    const hasError = refList.filter( (ref) => {
      return !ref.current.getValid();
    }).length;

    const data = formRef.current ? new FormData(formRef.current) : null;

    if(!hasError) {
      login(formData.name, formData.title);
    }
  }

  return <>
    <h1>Login</h1>
    <p>Bitte beliebigen Namen eingeben für die Verwendung in der Highscore Liste und Editor Modus ermöglichen</p>

    <form ref={formRef} action={''}>
      <div className={'mb-m'}>
        <div className={'form-row'}>
          <Input label={'Name'}
                 ref={nameRef}
                 prop={'name'}
                 type={'text'}
                 required={true}
                 value={formData.name}
                 setFormData={ (obj:Partial<FormData>) => setFormData(obj)} onChange={ (obj:Partial<FormData>) => setFormData(obj)} />
        </div>

        { /*<div className={'form-row'}>
          <Select label={'Titel'}
                  ref={titleRef}
                  prop={'title'}
                  values={["Herr", "Frau"]}
                  required={false}
                  value={formData.title}
                  setFormData={ (obj:Partial<FormData>) => setFormData(obj)} onChange={ (obj:Partial<FormData>) => setFormData(obj)} />
        </div> */}
      </div>

      <Button data-testid={'login-btn'} size={'small'} variant="contained" onClick={e => doLogin()}>Login</Button>
    </form>

    </>
}

export default Login;