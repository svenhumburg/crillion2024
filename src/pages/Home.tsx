import React from "react";
import {useTranslation} from "react-i18next";


function Home() {

  const { t } = useTranslation();

  return <>
    <div className={'header'}>
      <h2 className={'mb-m'}>{t('instruction')}</h2>
    </div>


    <dl className={'definition-list'}>

      <dt>
      </dt>
      <dd>{t('instructionControls')}</dd>

      <dt>
        <div className={ 'tile blue'}/>
        <div className={ 'tile green'}/>
      </dt>
      <dd>{t('instructionPoints')}</dd>

      <dt>
        <div className={ 'tile red'}/>
      </dt>
      <dd>{t('instructionLive')}</dd>

      <dt>
        <div className={ 'tile brick'}/>
      </dt>
      <dd>{t('instructionBlock')}</dd>

      <dt>
        <div className={ 'tile blue-change'}/>
        <div className={ 'tile green-change'}/>
      </dt>
      <dd>{t('instructionColor')}</dd>

      <dt>
        <div className={ 'tile blue-move'}/>
        <div className={ 'tile green-move'}/>
      </dt>
      <dd>{t('instructionMove')}</dd>
    </dl>
    </>
}

export default Home;