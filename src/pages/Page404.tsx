import React from "react";
import {Link} from "react-router-dom";

function Page404() {

  return <>
      <p>Seite konnte nicht gefunden werden</p>
      <Link to={'/'}>Zur Startseite</Link>
    </>
}

export default Page404;