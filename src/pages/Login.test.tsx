import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import Login from "./Login";

describe('renders the "Login" message', () => {

  it('empty login name should throw error message', () => {
    render(<Login />);
    const loginButton = screen.getByTestId('login-btn');
    const btn = screen.getByRole('button', {name: "Login"});
    const nameInput = screen.getByPlaceholderText('Name');
    fireEvent.change(nameInput, {target: {value: ''}});
    fireEvent.click(loginButton);
    expect(nameInput).toHaveClass('error');
  })

});