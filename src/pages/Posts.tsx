import React, {Dispatch, Reducer, useEffect, useMemo, useReducer} from "react";
import {useParams} from "react-router";
import {Comment, productList} from "../store/slices";
import {axiosInstance} from "../api/api";
import {useSelector, useDispatch} from "react-redux";
import {RootState} from "../store/store";
import {fetchProducts, getData} from "../api/action-thunks";
import {ThunkDispatch} from "@reduxjs/toolkit";

import moment from "moment";
import {Link} from "react-router-dom";
import dayjs from "dayjs";



enum CountActionKind {
  INCREASE = 'INCREASE',
  DECREASE = 'DECREASE',
}

// An interface for our actions
interface CommentAction {
  type: CountActionKind;
  payload: Array<Comment>;
}

function middleware (dispatchFunc:Dispatch<CommentAction>) {
  return async function(action: CommentAction) {
    const comments = await axiosInstance.getUserComments();
    dispatchFunc( { type: action.type, payload: comments.comments});
  }
}

function commentReducer(state:Array<Comment>, action:CommentAction) {
  state = action.payload;
  return state;
}

function Posts() {
  const { id } = useParams();
  const [comments, dispatchComments] = useReducer<Reducer<Array<Comment>, CommentAction>>( commentReducer, []);
  const middlewareDispatch = useMemo(() =>
    middleware(dispatchComments),
    [dispatchComments]);

  const a = moment('2024-01-25T10:30:00+12:00');
  const b = moment();
  const dateString = moment.unix(a.diff(b)).format("H:m:s");
  //console.log(dateString);

  const aa = dayjs('2024-10-10T10:30:00+12:00');
  const bb = dayjs();

  //console.log(aa.diff(bb, 'days'), aa.diff(bb, 'minutes')%60, aa.diff(bb, 'seconds')%60);
  //console.log(dayjs('2024-10-10').format('dddd, MMMM D, YYYY'));

  useEffect(() => {
    middlewareDispatch({type: CountActionKind.INCREASE, payload: []});
    return () => {
      //unmount
    }
  }, [middlewareDispatch])


  const dispatch = useDispatch<ThunkDispatch<any, any, any>>();
  let productsList = useSelector( productList);

  //console.log(productsList);
  if (id && productsList) {
    productsList = productsList.filter( (p) => {return p.id === parseInt(id)});
  }




  useEffect( () => {
    //dispatch(removeProduct(2));
    if(!productsList) {
      //Variant 1 and 2
      dispatch(fetchProducts());
      dispatch(getData());
    }
  }, [])


  const products = useSelector((state:RootState) => state.common.productResult);

  //console.log(products);


  /*const editorRef = useRef<any>(null);
  const [dragActive, setDragActive] = React.useState(false);

  const logga = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };

  // ref

  // handle drag events
  const handleDrag = function(e:any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.type === "dragenter" || e.type === "dragover") {
      setDragActive(true);
    } else if (e.type === "dragleave") {
      setDragActive(false);
    }
  };

  // triggers when file is dropped
  const handleDrop = function(e:any) {
    console.log(e);
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      console.log(e.dataTransfer.files);
      // handleFiles(e.dataTransfer.files);
    }
  };

  // triggers when file is selected with click
  const handleChange = function(e:any) {
    e.preventDefault();
    if (e.target.files && e.target.files[0]) {
      // handleFiles(e.target.files);
    }
  };

  const inputRef = React.useRef<any>(null);


  // triggers the input when the button is clicked
  const onButtonClick = () => {
    inputRef.current?.click();
  };*/

  return <>
    <span>New Posts {id}</span>

    { /*<form id="form-file-upload" onDragEnter={handleDrag} onSubmit={(e) => e.preventDefault()}>
      <input ref={inputRef} type="file" id="input-file-upload" multiple={true} onChange={handleChange} />
      <label id="label-file-upload" htmlFor="input-file-upload" className={dragActive ? "drag-active" : "" }>
        <div>
          <p>Drag and drop your file here or</p>
          <button className="upload-button" onClick={onButtonClick}>Upload a file</button>
        </div>
      </label>
      { dragActive && <div id="drag-file-element" onDragEnter={handleDrag} onDragLeave={handleDrag} onDragOver={handleDrag} onDrop={handleDrop}></div> }
    </form>

    <Editor
      onClick={ () => logga()}
      apiKey={'55207jj8zp6454zj7ul8jsn8wbg0ghw6923ca9jbhxqjzgoy'}
      onInit={(evt, editor) => editorRef.current = editor}
      initialValue="<p>This is the initial content of the editor.</p>"
      init={{
        height: 400,
        menubar: false,
        plugins: [
          'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
          'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
          'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
          'fontsize bold italic forecolor link | alignleft aligncenter alignright alignjustify | bullist numlist indent outdent | ' +
          'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
        font_size_formats: '10px 12px 14px 16px 18px 20px 24px 36px 48px',
      }}
    /> */}

    {<div>
      {productsList &&
        productsList.map( (c) => {
        return <p key={c.id}><Link to={'/posts/' + c.id}>{c.title}</Link></p>
      })}
      {comments.map( (c) => {
        return <p key={c.id}>{c.body}</p>
      })}
    </div> }
    </>
}

export default Posts;