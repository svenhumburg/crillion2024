import React from 'react';
import {fireEvent, render, screen, waitForElementToBeRemoved} from '@testing-library/react';
import Editor from "./Editor";
import {BrowserRouter} from "react-router-dom";
import { initReactI18next } from 'react-i18next';
import i18next from "i18next";
import I18nextBrowserLanguageDetector from "i18next-browser-languagedetector";

i18next.use(I18nextBrowserLanguageDetector).use(initReactI18next).init({
  lng: 'de',
  fallbackLng: 'de',
  resources: {
    de: {
      translation: {}
    },
    en: {
      translation: {}
    }
  }
});


const MockEditor = () => {
  return (
    <BrowserRouter>
      <Editor/>
    </BrowserRouter>
  )
}

describe('renders Editor Component', () => {

  it('ball set successfully', async() => {
    const {container} = render(<MockEditor/>);
    const selectBlueBall = container.querySelector('[class*="tile ball blue"]');
    expect(selectBlueBall).not.toHaveClass('selected');
    // select blue ball
    if (selectBlueBall) {
      fireEvent.click(selectBlueBall);
      expect(selectBlueBall).toHaveClass('selected');
    }

    const tiles = await screen.findAllByTestId('canvas-tiles');

    // check if tiles have correct length
    let tileList = tiles[0].querySelectorAll('.tile');
    expect(tileList.length).toBe(286);

    // add blue ball and check if ball set
    fireEvent.mouseDown(tileList[0]);

    const ballSet =  await screen.findByTestId('ball-set');
    expect(ballSet.innerHTML).toBe("yes");

    // select ball on differnet position and check if only one ball exists
    fireEvent.mouseDown(tileList[1]);

    let tileWithBall = tiles[0].querySelectorAll('.tile.ball');
    expect(tileWithBall.length).toBe(1);

    // select blue tile
    const selectBlueTile = container.querySelector('.tile.blue');
    if (selectBlueTile) {
      fireEvent.click(selectBlueTile);
    }

    // override ball and set blue tile instead
    fireEvent.mouseDown(tileList[1]);

    // expect ball has been removed
    tileWithBall = tiles[0].querySelectorAll('.tile.ball');
    expect(tileWithBall.length).toBe(0);
    expect(ballSet.innerHTML).toBe("no");

    const pointsSet =  await screen.findByTestId('points-set');
    expect(pointsSet.innerHTML).toBe("yes");



   // const tiles =  container.querySelector(".canvas .tile");
    //console.log(tiles)

    //console.log(tiles.length, tiles);

    //fireEvent.click()
    //const errorMsg = screen.getByText('Bitte Name eingeben');
    //expect(errorMsg).toBeInTheDocument();
  })

});