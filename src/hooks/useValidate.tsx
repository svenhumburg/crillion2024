import {useMemo} from "react";

export const useValidate = ():{doValidate(input:HTMLInputElement):void, isValid(value:string):boolean} =>  {
  function doValidate(input:any):void {
    // TODO implementation
  }

  function isValid(value:string):boolean{
    return value !== "";
  }

  return {
    doValidate,
    isValid
  }
}