import {
  BALL_LIST,
  COLS, CONVERTER_BRICK_LIST, MOVER_BRICK_LIST,
  POINT_BRICK_LIST,
  ROWS,
  TILE_HEIGHT,
  TILE_TYPE_LIST,
  TILE_WIDTH,
  TileValues
} from "../constants";
import React, {RefObject, useMemo} from "react";

export const useTileManager = ():{ getTileByValue(val:string):TileValues, getPreview(levelStr:string):Array<any>, isPointTile(value:string):boolean, isConverterTile(value:string):boolean, isMoverTile(value:string):boolean, isBall(value:string):boolean, getOrCreateRef(id:number):RefObject<unknown> } =>  {

  const references:{[key: number]: RefObject<unknown>} = {};

  function getTileByValue(val:string):TileValues {
    const obj = TILE_TYPE_LIST.filter(t => {
      if (val === t.value) {
        return t;
      }
    });
    return obj[0];
  }

  function getPreview(levelStr:string) {
    let count = 0;
    const tiles = [];
    for (let y = 0; y < ROWS; y++) {
      for (let x = 0; x < COLS; x++) {
        try {
          const val = levelStr.charAt(count++);
          const isBallType = isBall(val);
          const tile = {
            type: getTileByValue(val),
            left: (x * TILE_WIDTH) + (isBallType ? Math.round(TILE_WIDTH/2) : 0),
            top: (y * TILE_HEIGHT)  + (isBallType ? Math.round(TILE_HEIGHT/2) : 0),
            x: x,
            y: y,
            id: x + "_" + y,
            key: x + "_" + y
          };
          tiles.push(tile);
        } catch (e) {
          console.error(e);
        }

      }
    }
    return tiles;
  }

  const isPointTile = (type:string) => {
    return POINT_BRICK_LIST.indexOf(type) !== -1
  }

  function isConverterTile(type:string) {
    return CONVERTER_BRICK_LIST.indexOf(type) !== -1
  }

  function isMoverTile(type:string) {
    return MOVER_BRICK_LIST.indexOf(type) !== -1
  }

  function isBall(value:string) {
    return BALL_LIST.indexOf(value) !== -1;
  }

  const getOrCreateRef = (id: number) => {
    if (!references.hasOwnProperty(id)) {
      references[id] = React.createRef();
    }
    return references[id];
  }

  const methods = useMemo( () => ({
    getTileByValue,
    getPreview,
    isPointTile,
    isConverterTile,
    isMoverTile,
    isBall,
    getOrCreateRef
  }), [getTileByValue, getPreview, isPointTile, isConverterTile, isMoverTile, isBall, getOrCreateRef] )

  return methods;
}