import {Level, LevelListType} from "../constants";

export const useLevels = ():{ getGameLevels(levels:Level[]):Level[], getCustomLevels(levels:Level[]):Level[], getLevelById(levels:Level[], id:number):Level[] } =>  {
  function getGameLevels(levels: Level[]):Level[]{
    return levels.filter( (lev:Level) => {
      return lev.levelType === LevelListType.GAMELEVELS;
    })
  }

  function getCustomLevels(levels: Level[]):Level[]{
    return levels.filter( (lev:Level) => {
      return lev.levelType === LevelListType.CUSTOMLEVELS;
    })
  }

  function getLevelById(levels: Level[], id:number):Level[]{
    return levels.filter( (lev:Level) => {
      return lev.id === id;
    })
  }

  return {
    getGameLevels,
    getCustomLevels,
    getLevelById
  }
}