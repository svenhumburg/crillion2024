import React, {lazy, Suspense} from 'react';
import {useRoutes} from "react-router";
import Home from "./pages/Home";
import Posts from "./pages/Posts";
import Page404 from "./pages/Page404";
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import HomeLayout from "./layouts/HomeLayout";
import PrivateLayout from "./layouts/PrivateLayout";

const Game = lazy(() => import("./pages/Game"));
const Editor = lazy(() => import("./pages/Editor"));


function App() {

  const routes = useRoutes([
    {
      path: "/",
      element: <HomeLayout />,
      children: [
        {
          path: "/",
          element: <Home/>
        },
        {
          path: "game/:id?",
          element: <Suspense fallback={<div>Loading...</div>}><Game/></Suspense>
        },
        {
          path: "editor",
          element: <Suspense fallback={<div>Loading...</div>}><Editor/></Suspense>
        },
        {
          path: "login",
          element: <Login/>
        },
        {
          path: "*",
          element: <Page404/>
        }
      ]
    },
    {
      element: <PrivateLayout />,
      children: [
        {
          path: "posts/:id?",
          element: <Posts/>
        },
        {
          path: "posts",
          element: <Posts/>
        },
        {
          path: "profile",
          element: <Profile/>
        }
      ]
    },
    {
      path: '*',
      element: <Page404/>
    }
  ]);

  return routes;
}

export default App;