import React from "react";
import {Outlet} from "react-router";
import Navigation from "./Navigation";

function HomeLayout() {

  return <>
      <header>
        <Navigation/>
      </header>
      <Outlet/>
      <footer></footer>
  </>
}

export default HomeLayout;
