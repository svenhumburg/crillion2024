import React from "react";
import {useHref, useLocation} from "react-router";
import {DEV} from "../constants";
import {useAuth} from "../AuthProvider";
import LoginIcon from '@mui/icons-material/Login';
import {NavLink} from "react-router-dom";
import {FlagIcon} from "react-flag-kit";
import {useTranslation} from "react-i18next";

function Navigation() {
  const location = useLocation();
  const basename = DEV ? "" : useHref("/");
  const { user, logout } = useAuth();
  const { t, i18n } = useTranslation();

  const getActiveClass = (isActive:boolean) => {
    return isActive ? 'active' : 'inactive';
  }


  return <>
        <nav className={"main-navigation"}>
          <ul>
            <li><NavLink to='/' className={({isActive}) => getActiveClass(isActive)}>Home</NavLink></li>
            <li><NavLink to='/game' className={({isActive}) => getActiveClass(isActive)}>Game</NavLink></li>
            {user && <>
                <li><NavLink to='/editor' className={({isActive}) => getActiveClass(isActive)}>Editor</NavLink></li>
                <li><NavLink to='/profile' className={({isActive}) => getActiveClass(isActive)}>Profil</NavLink></li>
                {/*<li><NavLink to='/posts' className={({ isActive }) => getActiveClass(isActive)}>Posts</NavLink></li> */}
                <li className={'last'}><a onClick={() => logout()}>{t('logout')}</a></li>
              </>
            }
            {!user &&
              <li className={'last'}>
                <a className={`${location.pathname === '/login' ? "active" : ""}`} href={basename + '/login'}><LoginIcon/>{t('login')}</a>
              </li>
            }
          </ul>

          {user &&
            <span className={'login-info'}>{t('loggedin')} {user}</span>
          }

          <div className={'lang-toggle'}>
            <button onClick={() => i18n.changeLanguage('de')}>
              <FlagIcon code="DE" size={1} />
            </button>
            <button onClick={() => i18n.changeLanguage('en')}>
              <FlagIcon code="GB" size={1} />
            </button>
          </div>

        </nav>
  </>
}

export default Navigation;
