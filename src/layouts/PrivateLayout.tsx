import React from "react";
import {Navigate, Outlet} from "react-router";
import {useAuth} from "../AuthProvider";
import Navigation from "./Navigation";

function PrivateLayout() {
  const { user } = useAuth();

  if (!user) {
    return <Navigate to="/login" />;
  }

  return <>
      <Navigation/>
      <Outlet/>
  </>
}

export default PrivateLayout;