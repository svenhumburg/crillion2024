import React from 'react';
import ReactDOM from 'react-dom/client';
import "./sass/style.scss";
import App from './App';
import {Provider} from "react-redux";
import store from "./store/store";
import {BrowserRouter} from "react-router-dom";
import {AuthProvider} from "./AuthProvider";
import {BASE} from "./constants";
import './i18n';



const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
  <React.StrictMode>
      <Provider store={store}>
          <BrowserRouter basename={BASE} >
            <AuthProvider>
              <App/>
              { /*<Routes>
                <Route element={<HomeLayout/>}>
                  <Route path="/" element={<Home/>}></Route>
                  <Route path="/editor" element={<Editor/>}></Route>
                  <Route path="/login" element={<Login/>}></Route>
                  <Route path="*" element={<Page404/>}></Route>
                </Route>

                <Route element={<PrivateLayout/>}>
                  <Route path="/posts" element={<Posts/>}></Route>
                  <Route path="/profile" element={<Profile/>}></Route>
                </Route>
              </Routes> */}
            </AuthProvider>

          </BrowserRouter>
      </Provider>
  </React.StrictMode>
);
