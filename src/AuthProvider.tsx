import { createContext, useContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import {useLocalStorage} from "./hooks/useLocalStorage";
import React from "react";


const AuthContext = createContext({ user: null, login: (name:string, password: string) => {}, logout: () => {}});

export const useAuth = () => {
  return useContext(AuthContext);
};

export const AuthProvider = ({ children } : {children:any}) => {
  //const dispatch = useDispatch<ThunkDispatch<any, any, any>>();
  //const user = useSelector( (state:RootState) => state.common.user);
  const [user, setUser] = useLocalStorage('user', '');

  const navigate = useNavigate();

  const login = (name:string, password:string) => {
    //dispatch(setUser(name));
    setUser(name);
    navigate("/profile");
  };

  const logout = () => {
    //dispatch(setUser(null));
    setUser(null);
    navigate("/", { replace: true });
  };

  const authValues = useMemo(
    () => ({
      user,
      login,
      logout
    }),
    [user]
  );
  return <AuthContext.Provider value={authValues}>{children}</AuthContext.Provider>;
};
