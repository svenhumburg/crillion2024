import React from 'react';
import i18next from "i18next";
import {initReactI18next} from "react-i18next/initReactI18next";
import I18nextBrowserLanguageDetector from "i18next-browser-languagedetector";


const de = {
  instruction: 'Anleitung',
  login: "Anmelden",
  logout: "Abmelden",
  loggedin: "Eingeloggt als",
  points: "Punkte",
  lives: "Leben",
  blocks: "Blöcke",
  instructionControls: "Steuerung allgemein über PFEIL LINKS / PFEIL RECHTS Tasten",
  instructionPoints: "Blöcke, die Punkte bringen, sofern der Spielball die korrekte Farbe hat",
  instructionLive: "Block, der ein Leben kostet",
  instructionBlock: "Block, der nicht zerstört werden kann",
  instructionColor: "Farbwechsler",
  instructionMove: "Blöcke, die verschoben werden können, sofern der Spielball die korrekte Farbe hat",
  blockSelection: "Block-Auswahl",
  editorManual: "Nach Auswahl des Blocks kann der selectierte Block auf der Canvas-Fläche per Klick gesetzt werden. Alternativ mit gerückt haltender Maus mehrere Blöcke als Sequenz setzen.",
  ballSet: "Spielball gesetzt",
  pointsSet: "Punkte gesetzt",
  yes: "Ja",
  no: "Nein",
  messageLevelSuccess: 'Level geschafft',
  messageLevelValidated: 'Level wurde erfolgreich als spielbar validiert und kann als Game Level definiert werden.',
  messageLostLife: 'Leben verloren',
  messageGameOver: 'Game over',

};

const en = {
  instruction: 'Instructions',
  login: "Login",
  logout: "Logout",
  loggedin: "Logged in as",
  points: "Points",
  lives: "Lives",
  blocks: "Blocks",
  instructionControls: "General control via LEFT ARROW / RIGHT ARROW keys",
  instructionPoints: "Blocks that score points if the cue ball is the correct color",
  instructionLive: "Block that costs a life",
  instructionBlock: "Block that cannot be destroyed",
  instructionColor: "Color changer",
  instructionMove: "Blocks that can be moved as long as the cue ball is in the correct color",
  blockSelection: "Block selection",
  editorManual: "After selecting the block, the selected block can be placed on the canvas area with a click. Alternatively, place several blocks as a sequence by keeping the mouse moved.",
  ballSet: "Cue ball set",
  pointsSet: "Points set",
  yes: "Yes",
  no: "No",
  messageLevelSuccess: 'Level completed',
  messageLevelValidated: 'Level has been successfully validated as playable and can be defined as a game level',
  messageLostLife: 'Lost life',
  messageGameOver: 'Game over',
};

i18next.use(I18nextBrowserLanguageDetector).use(initReactI18next).init({
  lng: 'de',
  fallbackLng: 'de',
  resources: {
    de: {
      translation: de
    },
    en: {
      translation: en
    }
  }
});

export default i18next;