import {configureStore, ThunkAction, Action} from '@reduxjs/toolkit'
import commonReducer from "./slices";
import {axiosInstance} from "../api/api";

const store = configureStore({
  reducer: {
    common: commonReducer,
  },
  devTools: true,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      thunk: {
        extraArgument: {
          axios: axiosInstance,
          otherValue: 10
        }
      }
    })
})

export default store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, undefined, Action<string>>;