import {createSlice} from '@reduxjs/toolkit'
import {fetchProducts, fetchUserComments} from "../api/action-thunks";
import {RootState} from "./store";


export interface Product {
  id: number,
  title: string,
  description: string,
  price: number,
  discountPercentage: number,
  brand: string,
  category: string,
  images: Array<string>,
  rating: number,
  stock: number,
  thumbnail: string
}

export const ProductList:Product[] = [
  {id:1,
    title:"iPhone 9",
    description:"An apple mobile which is nothing like apple",
    price:549,
    discountPercentage:12.96,
    rating:4.69,
    stock:94,
    brand:"Apple",
    category:"smartphones",
    thumbnail:"https://cdn.dummyjson.com/product-images/1/thumbnail.jpg",
    images:["https://cdn.dummyjson.com/product-images/1/1.jpg"]
  },
  {id:2,
    title:"iPhone 10",
    description:"An apple mobile which is nothing like apple",
    price:549,
    discountPercentage:12.96,
    rating:4.69,
    stock:94,
    brand:"Apple",
    category:"smartphones",
    thumbnail:"https://cdn.dummyjson.com/product-images/1/thumbnail.jpg",
    images:["https://cdn.dummyjson.com/product-images/1/1.jpg"]
  }
];

export interface ProductResult {
  limit: number
  products: Array<Product>,
  skip: number,
  total: number
}

export interface Comment {
  id: number
  body: string,
  postId: number,
  user: any
}

export interface UserCommentResult {
  limit: number
  comments: Array<Comment>,
  skip: number,
  total: number
}

export const commonSlice = createSlice({
  name: 'common',
  initialState: {
    user: null,
    productResult: {} as ProductResult,
    productlist: ProductList,
    userCommentResult: {} as UserCommentResult,
    status: 'idle',
    loadState: false,
  },
  reducers: {
    setData: (state, action) => {
      state.productResult = action.payload;
    },

    setUser: (state, action) => {
      state.user = action.payload;
    },

    removeProduct: (state, action) => {
      const index = state.productlist.findIndex( (product) => {
        return product.id === action.payload;
      });

      if(index !== -1){
        state.productlist.splice(index,1);
      }
    }
  },
  extraReducers(builder) {
    builder
      .addCase(fetchProducts.pending, (state, action) => {
        state.status = 'loading';
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = 'succeeded';
        //console.log(state.status);
        // Add any fetched posts to the array
        //state.data = state.data.concat(action.payload)
        state.productResult = action.payload as ProductResult;
        state.productResult.products.forEach( product => {
          //console.log(product.description);
        })

      })
      .addCase(fetchProducts.rejected, (state, action) => {
        state.status = 'failed'
        //state.data = action.error.message
      })
      .addCase(fetchUserComments.pending, (state, action) => {
        state.status = 'loading';
      })
      .addCase(fetchUserComments.fulfilled, (state, action) => {
        state.status = 'succeeded';
        //console.log(state.status);
        // Add any fetched posts to the array
        //state.data = state.data.concat(action.payload)
        state.productResult = action.payload as ProductResult;
        state.productResult.products.forEach( product => {
          //console.log(product.description);
        })

      })
      .addCase(fetchUserComments.rejected, (state, action) => {
        state.status = 'failed'
        //state.data = action.error.message
      })
  }
})

export const {setData, setUser,removeProduct} = commonSlice.actions

export const productList = (state:RootState) => state.common.productResult.products

export function selectProduct(state: RootState): (id?:number) => Product | null {
  return (id?:number): Product | null => {
    const product = productList(state).find((product) => product.id === id);
    if(!product) {
      return null;
    }
    return product;
  }

}

export default commonSlice.reducer