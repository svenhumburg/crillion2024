import {axiosInstance} from "./api";
import {setData} from "../store/slices";
import {createAsyncThunk} from "@reduxjs/toolkit";
import store, {RootState} from "../store/store";

// Variant 1: with redux async thunks including option of progress
export const fetchProducts = createAsyncThunk(
  'common/fetchProducts',
  async () => {
    const response = await axiosInstance.geData();
    return response;
  }
)

export const fetchUserComments = createAsyncThunk(
  'common/fetchUserComments',
  async () => {
    const response = await axiosInstance.getUserComments();
    return response;
  }
)

// Variant 2 with thunks
export const getData = () => async(dispatch:any, getState:any, extraArgs:any) => {
  const { axios, otherValue } = extraArgs;
  const state = getState();
  console.log(state);
  const data = await axios.geData();
  dispatch(setData(data));
}
