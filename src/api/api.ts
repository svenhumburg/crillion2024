import {AxiosError, AxiosResponse} from "axios";
import {SERVER} from "../constants";
import axios from 'axios';

const client = axios.create({
  baseURL: SERVER,
  timeout: 10000,
});

class Api {
  async geData(){
    return await client.get( 'https://dummyjson.com/products', {})
      .then( (response:AxiosResponse<any>) => {
          //console.log(response);
          return response.data;
      }).catch(function (error:AxiosError) {
          console.log(error + "");
      });
  }

  async getUserComments() {
    return await client.get('https://dummyjson.com/comments', {})
      .then ((response:AxiosResponse<any>) => {
        return response.data;
      }).catch(function (error:AxiosError) {
        console.log(error + "");
      });
  }
}

//export default Api;

export const axiosInstance = new Api();