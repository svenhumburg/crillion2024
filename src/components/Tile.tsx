import React, {forwardRef, MouseEventHandler, useEffect, useImperativeHandle, useState} from 'react';
import {produce} from "immer";
import PropTypes from "prop-types";
import {
  RectSet,
  TILE_HEIGHT,
  TILE_WIDTH,
  TileValues
} from "../constants";


const initialRect:RectSet = {
  top: {
    x: 0,
    y: 0,
    width: TILE_WIDTH - 8,
    height: 4
  },
  right: {
    x: 0,
    y: 0,
    width: 4,
    height: TILE_HEIGHT - 8
  },
  bottom: {
    x: 0,
    y: 0,
    width: TILE_WIDTH - 8,
    height: 4
  },
  left: {
    x: 0,
    y: 0,
    width: 4,
    height: TILE_HEIGHT - 8
  }
}

const Tile = forwardRef( ({onChangeTile,
                onStartEditMode,
                onStopEditMode,
                type,
                left,
                top} :
                {
                  onChangeTile?:MouseEventHandler,
                  onStartEditMode?:MouseEventHandler,
                  onStopEditMode?:MouseEventHandler,
                  type:TileValues,
                  left:number,
                  top:number}, ref:any )  => {

  const [rect, setRect] = useState(initialRect);

  useEffect(() => {
    setRect( (prev:RectSet) => {
      return produce(prev, (draftState) => {
        draftState.top.x = left + 4;
        draftState.top.y = top;
        draftState.right.x = TILE_WIDTH - 4 + left;
        draftState.right.y = top + 4;
        draftState.bottom.x = left + 4;
        draftState.bottom.y = TILE_HEIGHT - 4 + top;
        draftState.left.x = left;
        draftState.left.y = top + 4;
        return draftState;
      });
    });

    return () => {
    }
  }, [left, top]);

  useImperativeHandle(ref, () => ({
    getRect() {
      return rect;
    }
  }));

  return (
    <div ref={ref} onMouseEnter={onChangeTile} onMouseDown={onStartEditMode} onMouseUp={onStopEditMode} className={ `tile ${type.classname}` } style={{ 'left': left, 'top': top}}></div>
  )
});

Tile.propTypes = {
  onChangeTile: PropTypes.func,
  onStartEditMode: PropTypes.func,
  onStopEditMode: PropTypes.func,
  type: PropTypes.shape(
    {
      classname: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }
  ).isRequired,
  left: PropTypes.number.isRequired,
  top: PropTypes.number.isRequired
}

export default Tile

