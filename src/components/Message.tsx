import React from 'react';

const Message = ({classname, messageText}: {classname:string, messageText: string}) => {

  return (<section className={ 'message ' + classname}>
    <div>{messageText}</div>
  </section>)
}

export default Message
