import React from 'react';
import {Alert, Button, Card} from "@mui/material";
import Tile from "./Tile";
import {Delete} from "@mui/icons-material";
import {useNavigate} from "react-router-dom";
import {useTileManager} from "../hooks/useTileManager";
import PropTypes from "prop-types";
import {defaultGameLevels, Level, LevelListType} from "../constants";
import {useLocalStorage} from "../hooks/useLocalStorage";

const LevelPreviewList = ({levels, levelType, selectLevel, setDialogState, changeToGameLevel}: {levels:Level[], levelType: string, selectLevel: Function, setDialogState:Function, changeToGameLevel:Function}) => {
  const navigate = useNavigate();
  const { getPreview } = useTileManager();

  return (<>
    { levels.length === 0 && <Alert severity={'info'}>Es sind aktuelle keine gespeicherten Levels vorhanden</Alert> }
    {levels.length > 0 &&
      <section className={'preview-outer'}>
        <div className={'preview-grid'}>

          {levels.map((level: Level, i: number) => {
            return <div className={'preview'} key={'preview_' + i}>
              <Card variant={'outlined'} className='canvas small'>
                {getPreview(level.levelstr).map((t: any, index) => {
                    return <Tile key={t.key}
                                 left={t.left}
                                 top={t.top}
                                 type={t.type}/>
                  }
                )}
              </Card>
              <div className={'cta-wrapper'}>
                <Button size={'small'} variant="contained"
                        onClick={() => navigate('/game/' + level.id + '/')}>Spielen</Button>
                <Button size={'small'} variant="contained" onClick={(e) => selectLevel(level)}>Editieren</Button>
                <Button size={'small'} startIcon={<Delete/>} variant="contained" color="error"
                        onClick={() => setDialogState({
                          message: "Level wirklich löschen?",
                          payload: level
                        })}>löschen</Button>
                { level.proofed === true && levelType === LevelListType.CUSTOMLEVELS && <Button size={'small'} variant="contained" onClick={() =>{ changeToGameLevel(level.id) } }>Game Level</Button> }
              </div>
            </div>
          })}
        </div>
      </section>
    }
  </>)
}

export default LevelPreviewList

LevelPreviewList.propTypes = {
  levels: PropTypes.arrayOf(PropTypes.any),
  levelType: PropTypes.string,
  selectLevel: PropTypes.func.isRequired,
  setDialogState: PropTypes.func.isRequired,
  changeToGameLevel: PropTypes.func.isRequired,
}
