import React, {forwardRef, useEffect, useImperativeHandle, useState} from "react";
import PropTypes from "prop-types";
import Tile from "./Tile";
import {useValidate} from "../hooks/useValidate";
import {Playstate} from "../constants";


const Select = forwardRef(({label, prop, values, required, value, setFormData, onChange}: {label:string, prop:string, values: string[], required: boolean, value:string, setFormData:Function, onChange: Function}, ref:any ) => {

  const {doValidate, isValid} = useValidate();
  const [touched, setTouched] = useState(false);

  const handleOnChange = (e:any) => {
    setTouched(true);
    setFormData({[prop]: e.currentTarget.value});
    doValidate(e.currentTarget);
  }

  useImperativeHandle(ref, () => ({
    getValid() {
      setTouched(true);
      return !required || isValid(value);
    }
  }));

  return <>
    <select className={!isValid(value) && touched && required ? 'error' : ''}
            value={value}
            name={prop}
            placeholder={label}
            onChange={e => handleOnChange(e) }>
              <option value={''}>Auswahl</option>
              {values.map( val => {
                return <option value={val} key={val}>{val}</option>
              })}
    </select>
    <span className={'error-msg'}>Bitte {label} eingeben</span>
    </>
});

export default Select

Select.propTypes = {
  label: PropTypes.string.isRequired,
  prop: PropTypes.string.isRequired,
  values: PropTypes.array.isRequired,
  required: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired,
  setFormData: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
}