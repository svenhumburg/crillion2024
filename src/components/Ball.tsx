import React, {forwardRef, useEffect, useReducer, useRef, useImperativeHandle, useCallback} from 'react';
import PropTypes, {bool, number} from "prop-types";
import {
 TILE_HEIGHT,
  TILE_WIDTH,
} from "../constants";

interface ballData {
  start_x:number,
  start_y:number,
  horizontal_move:number,
  is_active:boolean
}

const Ball =  forwardRef(({
                onPositionChange,
                left,
                top,
                direction,
                classname,
                horizontalFactor} :
                { onPositionChange:Function,
                  left:number,
                  top:number,
                  direction:number,
                  classname:string,
                  horizontalFactor:number}, ref) => {

  const STEP = 4;
  const [ballData, setBallData] = useReducer((state:ballData, item:Partial<ballData>) => { return {...state, ...item } }, {start_x: left, start_y: top, horizontal_move: 0, is_active: false});
  let moveInterval = useRef<any>(null);

  useEffect( () => {
    reset();
    initMovement();

    window.addEventListener('keydown', (e:KeyboardEvent) => {
      if (e.code === 'ArrowRight') {
        setBallData({horizontal_move: 1});
      } else if (e.code === 'ArrowLeft') {
        setBallData({horizontal_move: -1});
      }
    });

    window.addEventListener('keyup', (e:KeyboardEvent) => {
      setBallData({horizontal_move: 0});
    });

    return () => {
      reset();
      window.removeEventListener('keydown', (e) => {});
      window.removeEventListener('keyup', (e) => {});
    }
  }, []);

  useImperativeHandle(ref, () => ({
    doControl(code:string) {
      switch (code) {
        case "left": setBallData({horizontal_move: -1}); break;
        case "right": setBallData({horizontal_move: 1}); break;
        default: setBallData({horizontal_move: 0});
      }
    }
  }));


  useEffect( () => {
    setMoveInterval();
  }, [top, left]);

  const initMovement = useCallback( () => {
    setBallData({is_active: true});
    setMoveInterval();
  }, [])

  const setMoveInterval = useCallback( () => {
    reset();
    moveInterval.current = setInterval( e => {
      const leftMove = ballData.horizontal_move === 0 ? left : left + (STEP * ballData.horizontal_move * horizontalFactor);
      const topMove = top + (direction * STEP);
      onPositionChange(topMove, leftMove, Math.floor(left/TILE_WIDTH), Math.floor(top/TILE_HEIGHT));
    }, 12);
  }, [left, top])

  const reset = () =>  {
    clearInterval(moveInterval.current);
  }

  return (
    <div className={ `${classname}` } style={{ left: left, top: top }}></div>
  )
})

Ball.propTypes = {
  onPositionChange: PropTypes.func.isRequired,
  left: PropTypes.number.isRequired,
  top: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  classname: PropTypes.string.isRequired
}


export default Ball
