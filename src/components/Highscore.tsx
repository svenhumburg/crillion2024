import React from 'react';

import {HighscoreEntry} from "../constants";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import {Paper} from "@mui/material";
import {useLocalStorage} from "../hooks/useLocalStorage";

const Highscore = () => {
  const [highscore, setHighscore] = useLocalStorage('highscore', []);

  return (<section>

    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 250 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell align="right">Score</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {highscore.slice(0, 10).map((row:HighscoreEntry) => (
            <TableRow key={Math.random().toString(16).slice(2)} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
              <TableCell>{row.name}</TableCell>
              <TableCell align="right">{row.score}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>

  </section>)
}

export default Highscore
