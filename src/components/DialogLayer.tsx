import React from 'react';
import {DialogState} from "../pages/Editor";
import {Button} from "@mui/material";

const DialogLayer = ({dialogState, onConfirm, onCancel}: {dialogState: DialogState, onConfirm:Function, onCancel:Function}) => {

  return (<section className={ 'dialog-layer'}>
    <div className={'layer-content'}>
      <div>{dialogState.message}</div>
      <div className={'cta-wrapper'}>
        <Button size={'small'} variant="contained" onClick={ () => onConfirm(dialogState.payload)}>Bestätigen</Button>
        <Button size={'small'} variant="contained" color="error" onClick={ () => onCancel() }>Abbrechen</Button>
      </div>
    </div>
  </section>)
}

export default DialogLayer
