import React, {forwardRef, useImperativeHandle, useState} from "react";
import PropTypes from "prop-types";
import {useValidate} from "../hooks/useValidate";


const Input = forwardRef(({label, prop, type, required, value, setFormData, onChange}: {label:string, prop:string, type: string, required: boolean, value:string, setFormData:Function, onChange: Function}, ref:any ) => {

  const {doValidate, isValid} = useValidate();
  const [touched, setTouched] = useState(false);

  const handleOnChange = (e:React.FormEvent<HTMLInputElement>) => {
    setTouched(true);
    setFormData({[prop]: e.currentTarget.value});
    doValidate(e.currentTarget);
  }

  useImperativeHandle(ref, () => ({
    getValid() {
      setTouched(true);
      return !required || isValid(value);
    }
  }));

  return <>
    <input className={!isValid(value) && touched && required ? 'error' : ''}
           type={type} value={value}
           name={prop}
           placeholder={label}
           onChange={e => handleOnChange(e) }/>
    <span className={'error-msg'}>Bitte {label} eingeben</span>
    </>
});

export default Input

Input.propTypes = {
  label: PropTypes.string.isRequired,
  prop: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired,
  setFormData: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
}