import React, {useEffect} from 'react';
import {Product} from "../store/slices";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

const SingleProduct = ({product, test} : { product:Product, test?:number}) => {

  useEffect(() => {
    //console.log("Mount");

    return () => {
      //console.log("unmount");
    }
  }, [])

  return (
    <Link to={'/posts/' + product.id} >{product.id}</Link>
  )
}

SingleProduct.propTypes = {
  test: PropTypes.number,
  //product: PropTypes.object.isRequired,
  product: PropTypes.shape(
    {
      id: PropTypes.number,
      title: PropTypes.string,
      description: PropTypes.string,
      price: PropTypes.number,
      discountPercentage: PropTypes.number,
      brand: PropTypes.string,
      category: PropTypes.string,
      images: PropTypes.arrayOf(PropTypes.string),
      rating: PropTypes.number,
      stock: PropTypes.number,
      thumbnail: PropTypes.string.isRequired,
    }
  ).isRequired
}


export default SingleProduct
