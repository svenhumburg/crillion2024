import React, {forwardRef, useImperativeHandle} from 'react';
import Tile from "./Tile";
import {Collision, Rect, TILE_BRICK_RED, TILE_SPACE, TileValues} from "../constants";
import {useTileManager} from "../hooks/useTileManager";
import PropTypes from "prop-types";




const withCollisionCheck = (Component:any) => {
  return forwardRef(({onPoints, onLostLife, onCollisionCheck, onChangeColor, left, top, xpos, ypos, type}: {onPoints:Function, onLostLife: Function, onCollisionCheck: Function, onChangeColor: Function, left:number, top:number, xpos:number, ypos:number, type:TileValues}, ref:any) => {

    const {isPointTile, isConverterTile, isMoverTile} = useTileManager();
    const ref2:any = React.createRef();
    const Comp = <Component ref={ref2} onPoints={onPoints} onLostLife={onLostLife} onCollisionCheck={onCollisionCheck} onChangeColor={onChangeColor} left={left} top={top} xpos={xpos} ypos={ypos} type={type}/>;

    useImperativeHandle(ref, () => ({
      checkCollision(rect2:Rect, ball_color:string) {
        let collision = 0;
        const rect = ref2.current.getRect();

        if (type.value !== TILE_SPACE.value) {
          if (
            rect.top.x < rect2.x + rect2.width &&
            rect.top.x + rect.top.width > rect2.x &&
            rect.top.y < rect2.y + rect2.height &&
            rect.top.height + rect.top.y >rect2.y) {
            collision = Collision.TOP;
          } else if (
            rect.bottom.x < rect2.x + rect2.width &&
            rect.bottom.x + rect.bottom.width > rect2.x &&
            rect.bottom.y <rect2.y + rect2.height &&
            rect.bottom.height + rect.bottom.y >rect2.y) {
            collision = Collision.BOTTOM;
          }

          if (
            rect.left.x < rect2.x + rect2.width &&
            rect.left.x + rect.left.width > rect2.x &&
            rect.left.y <rect2.y + rect2.height &&
            rect.left.height + rect.left.y >rect2.y) {
            collision = Collision.LEFT;
          } else if (
            rect.right.x < rect2.x + rect2.width &&
            rect.right.x + rect.right.width > rect2.x &&
            rect.right.y <rect2.y + rect2.height &&
            rect.right.height + rect.right.y >rect2.y) {
            collision = Collision.RIGHT;
          }
        }

        if (collision && type.classname.indexOf("hit") === -1) {
          if (type.value === TILE_BRICK_RED.value) {
            onLostLife();
          } else if (isPointTile(type.value) && type.ballValueRef === ball_color) {
            onPoints();
          } else if (isMoverTile(type.value) && type.ballValueRef === ball_color) {
            onCollisionCheck(xpos, ypos, collision, type);
          } else if (isConverterTile(type.value)) {
            onChangeColor(type.ballValueRef);
          }
        }

        return collision;
      }
    }));

    return Comp
  });
};

const GameTile = withCollisionCheck(Tile);

GameTile.propTypes = {
  onLostLife: PropTypes.func.isRequired,
  onPoints: PropTypes.func.isRequired,
  onCollisionCheck: PropTypes.func.isRequired,
  onChangeColor: PropTypes.func.isRequired,
  type: PropTypes.shape(
    {
      classname: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }
  ).isRequired,
  left: PropTypes.number.isRequired,
  top: PropTypes.number.isRequired,
  xpos: PropTypes.number.isRequired,
  ypos: PropTypes.number.isRequired
}

export default GameTile

